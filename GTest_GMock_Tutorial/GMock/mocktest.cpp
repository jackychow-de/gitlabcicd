#include "pch.h"
#include <iostream>
#include <vector>

using namespace std;
using ::testing::AtLeast;
using ::testing::Return;
using ::testing::_;


/*
class MockTest {
public:
    MOCK_METHOD(void, SomeMethod, ());
};

TEST(TestCaseName, TestName) {
    MockTest mock;
    EXPECT_CALL(mock, SomeMethod);
    mock.SomeMethod();
    EXPECT_EQ(1, 1);
    EXPECT_TRUE(true);
}
*/

class DataBaseConnect {
public:
    virtual bool login(string username, string password)
    {
        return true;
    }
    virtual bool logout(string username) { return true; }
    virtual int fetchRecord() { return -1; }
};

class MocKDB: public DataBaseConnect{

public:
    MOCK_METHOD0(fetchRecord, int());
    MOCK_METHOD1(logout, bool(string username));
    MOCK_METHOD2(login, bool(string username, string password));
};


class MyDatabase{
    DataBaseConnect& dbC;
public:
    MyDatabase(DataBaseConnect & _dbC) : dbC(_dbC){}
    int Init(string username, string password) {
        if (dbC.login(username, password) != true) {
            cout << "DB FAILURE" << endl; return -1;
        }
        else {
            cout << "DB SUCCESS" << endl; return 1;

        }
    }


};

TEST(MyDBTest, LoginTest) {
    //Arrange
    MocKDB mdb;
    MyDatabase db(mdb);

    EXPECT_CALL(mdb, login("Terminator", "I 'm Back")).Times(1).WillOnce(Return(true));
    
	//Act
    int retValue = db.Init("Terminator", "I 'm Baack"); // return false with wrong statement

    //Assert
    EXPECT_EQ(retValue, 1);
 }
