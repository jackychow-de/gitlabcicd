﻿#include <iostream>

#include "pch.h"
using namespace std; 

class Check {
	int val;
public:
	Check() : val(0) {}
	void setValue(int newVal) { this->val = newVal; }
		int getVal() {
			return this->val;
		}

};	

//Test Fixtures
//Make sure the first letter of function name should be in Capital Letter
struct TF :public testing ::Test {
	Check* c1;
	void SetUp() { cout << "SETUP" << endl; c1 = new Check(); }
	void TearDown() { cout << "TEAR DOWN" << endl; delete c1;  }
};

	TEST_F(TF, TestName) {
	//Act
	c1->setValue(100);

	//Assert
	ASSERT_EQ(c1->getVal(), 100);
}


int main(int argc, char** argv) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}