#include <iostream>
#include <vector>; 
#include "pch.h"
using namespace std;

class Stack {
	vector<int> vstack = {};
public:
	void push(int value)
	{
		vstack.push_back(value); // push_back		adds an element to the end 		(public member function)
	}
	int pop() {
		if (vstack.size() > 0) {
			int value = vstack.back(); // access the last element 			(public member function)
			vstack.pop_back(); //removes the last element			(public member function)
			return value;
		}
		else {
			return -1;
		} 
		}
		int size() { return vstack.size();
	}
};

struct stackTest : public testing::Test {
	Stack sl;
	void SetUp() {
		int value[] = { 1,2,3,4,5,6,7,8,9 };
		for (auto& val : value) {  //will always be a reference. 
			sl.push(val);
		}
	}
	void TearDown() {}

};

//Test Fixtures
//Make sure the first letter of function name should be in Capital Letter
/*struct TF :public testing::Test {
	Check* c1;
	void SetUp() { cout << "SETUP" << endl; c1 = new Check(); }
	void TearDown() { cout << "TEAR DOWN" << endl; delete c1; }
};
*/


TEST_F(stackTest, PopTest) {
	int lastPoppedValue = 9;
	while (lastPoppedValue != 1)
		ASSERT_EQ(sl.pop(), lastPoppedValue--);
}

TEST_F(stackTest, sizeValidityTest) {
	int val = sl.size();
	for(val; val >0; val--)
		ASSERT_NE(sl.pop(), -2);
}