#include <iostream>

#include "pch.h"
using namespace std;

class RXClass {
	int baseValue;
public:
	RXClass(int _bv) : baseValue(_bv) {}
	void Increment(int byValue) {
		baseValue += byValue;
	}
	int getValue() { return baseValue; }

};

struct RXClassTest : public testing::Test {
	RXClass* rx;
	void SetUp() { cout << "SETUP" << endl; rx = new RXClass(88); }
	void TearDown() { cout << "TEAR DOWN" << endl; delete rx; }

};

//Test Fixtures
//Make sure the first letter of function name should be in Capital Letter
/*struct TF :public testing::Test {
	Check* c1;
	void SetUp() { cout << "SETUP" << endl; c1 = new Check(); }
	void TearDown() { cout << "TEAR DOWN" << endl; delete c1; }
};
*/


TEST_F(RXClassTest, increment_by_5) {
	
	//Act
	rx->Increment(5);

	//Assert
	ASSERT_EQ(rx->getValue(), 93);
}

