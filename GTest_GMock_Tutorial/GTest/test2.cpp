#include <iostream>

#include "pch.h"
using namespace std;

class MyClass {
	string id;
public:
	MyClass(string _id) : id(_id){}
	string GetId() { return id; }

};



//Test Fixtures
//Make sure the first letter of function name should be in Capital Letter
/*struct TF :public testing::Test {
	Check* c1;
	void SetUp() { cout << "SETUP" << endl; c1 = new Check(); }
	void TearDown() { cout << "TEAR DOWN" << endl; delete c1; }
};
*/


TEST (TestName, increment_by_5) {
	//Arrannge
	MyClass mc("root");
	
	//Act
	string value = mc.GetId();

	//Assert
	EXPECT_STREQ(value.c_str(), "root");
}

